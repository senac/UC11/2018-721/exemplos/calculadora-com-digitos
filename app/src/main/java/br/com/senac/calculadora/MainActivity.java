package br.com.senac.calculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private static final String ZERO = "0";
    private static final String PONTO = ".";
    private Button btn0;
    private Button btn1;
    private Button btn2;
    private Button btn3;
    private Button btn4;
    private Button btn5;
    private Button btn6;
    private Button btn7;
    private Button btn8;
    private Button btn9;
    private Button btnSomar;
    private Button btnSubtracao;
    private Button btnDivisao;
    private Button btnMultiplicacao;
    private Button btnPonto;
    private Button btnLimpar;
    private Button btnIgual;
    private TextView display;
    private boolean limpar;
    private double operando1;
    private double operando2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        limpar = false;

        initComponentes();


        btn0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                escreverDisplay(view);
            }
        });

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                escreverDisplay(view);
            }
        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                escreverDisplay(view);
            }
        });

        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                escreverDisplay(view);
            }
        });

        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                escreverDisplay(view);
            }
        });

        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                escreverDisplay(view);
            }
        });

        btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                escreverDisplay(view);
            }
        });

        btn7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                escreverDisplay(view);
            }
        });

        btn8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                escreverDisplay(view);
            }
        });

        btn9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                escreverDisplay(view);
            }
        });

        btnLimpar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                limparDisplay();
            }
        });

        btnPonto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                escreverDisplay(view);
            }
        });


        btnSomar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               calcular();


            }
        });

        btnIgual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                calcular();
        }
        });


    }


    private void calcular() {

        String numero = display.getText().toString();

        if (operando1 == 0) {
            operando1 = Double.parseDouble(numero);
            Log.i("#CALCULADORA", "Numero1:" + operando1);
        } else {
            operando2 = Double.parseDouble(numero);
            Log.i("#CALCULADORA", "Numero2:" + operando2);
        }
        limpar = true;

        if(operando1 != 0 && operando2 != 0) {
            if(operando2!=0) {
                operando1 = operando1 + operando2;
            }
            numero = String.valueOf(operando1);
            display.setText(numero);
            operando2 = 0;
        }


    }

    private void limparDisplay() {
        this.display.setText(ZERO);
        this.operando1 = 0;
        this.operando2 = 0;
    }


    private void escreverDisplay(View view) {

        Button button = (Button) view;
        String digito = button.getText().toString();
        String valorDisplay = display.getText().toString();

        if (valorDisplay.equals(ZERO) || limpar) {
            limpar = false;
            if (!digito.equals(PONTO)) {
                display.setText(digito);
            } else {
                if (!valorDisplay.contains(PONTO)) {
                    valorDisplay += PONTO;
                    display.setText(valorDisplay);
                }
            }
        } else {

            valorDisplay += digito;
            display.setText(valorDisplay);

        }


    }

    private void initComponentes() {
        btn0 = (Button) findViewById(R.id.btn0);
        btn1 = (Button) findViewById(R.id.btn1);
        btn2 = (Button) findViewById(R.id.btn2);
        btn3 = (Button) findViewById(R.id.btn3);
        btn4 = (Button) findViewById(R.id.btn4);
        btn5 = (Button) findViewById(R.id.btn5);
        btn6 = (Button) findViewById(R.id.btn6);
        btn7 = (Button) findViewById(R.id.btn7);
        btn8 = (Button) findViewById(R.id.btn8);
        btn9 = (Button) findViewById(R.id.btn9);
        btnSomar = (Button) findViewById(R.id.btnSomar);
        btnSubtracao = (Button) findViewById(R.id.btnSubtracao);
        btnDivisao = (Button) findViewById(R.id.btnDivisao);
        btnMultiplicacao = (Button) findViewById(R.id.btnMultiplicao);
        btnPonto = (Button) findViewById(R.id.btnPonto);
        btnLimpar = (Button) findViewById(R.id.btnLimpar);
        btnIgual = (Button) findViewById(R.id.btnIgual);
        display = (TextView) findViewById(R.id.display);
    }
}
